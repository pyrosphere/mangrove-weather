//
//  WeatherAPIClient.swift
//  weather
//
//  Created by Filipe Lemos on 09/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import Foundation

class WeatherAPIClient {
    
    var apiKey: String? = nil
    
    static let shared: WeatherAPIClient = {
        let apiKey = "6b8ac1e356cba2872443e1b921035e99"
        // FIXME: hardcoded api key, use info.plist ? might not be secure ...
        let instance = WeatherAPIClient(apiKey: apiKey)
        return instance
    }()

    init(apiKey: String? = nil) {
        self.apiKey = apiKey
    }
    
    enum Response<T, E: Error> {
        case success(T)
        case failure(E)
    }
    
    enum APIError: Error, CustomStringConvertible {
        case missingAPIKey
        case buildingURL
        case fetchingData(error: Error)
        case missingData
        case parsingJSON(error: Error)
        case badCode(code: String)
        
        var description: String {
            switch self {
            case .missingAPIKey:
                return "Error API Key has not been set"
            case .buildingURL:
                return "Error building URL"
            case .fetchingData(let error):
                return "Error fetching data with error: \(error.localizedDescription)"
            case .missingData:
                return "Error fetched no data"
            case .parsingJSON(let error):
                return "Error parsing JSON data with error: \(error.localizedDescription)"
            case .badCode(let code):
                return "Error in code returned from API: \(code)"
            }
            
        }
    }

    // Result structure for API calls to forecast5
    struct Forecast: Decodable {
        
        let code: String
        let city: City?
        let entries: [Entry]?

        enum CodingKeys: String, CodingKey {
            case code = "cod"
            case city
            case entries = "list"
        }

        struct City: Decodable {
            let id: Int
            let name: String
        }
        
        struct Entry: Decodable {
            let date: Date
            let temperature: Temperature
            let weather: WeatherState
            
            enum CodingKeys: String, CodingKey {
                case date = "dt"
                case temperature = "temp"
                case weather
            }
            
            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                let dt: Int = try values.decode(Int.self, forKey: .date)
                date = Date(timeIntervalSince1970: TimeInterval(dt))
                temperature = try values.decode(Temperature.self, forKey: .temperature)
                
                // parse weather
                let weathers:[Weather] = try values.decode([Weather].self, forKey: .weather)
                var parsedWeather = WeatherState.other
                for w in weathers {
                    let ws = WeatherState.from(code: w.id)
                    if ws != .other {
                        parsedWeather = ws
                        break
                    }
                }
                weather = parsedWeather
            }

            private struct Weather: Decodable {
                let id: Int
            }
        }
        
        struct Temperature: Decodable {
            let day: Float
            let min: Float
            let max: Float
        }
        
    }
    
    func forecast(settings: Settings, completion: ((Response<Forecast, APIError>) -> Void)?) {
        forecast(settings.location, temperature: settings.temperatureUnit, count: settings.forecastDays, completion: completion)
    }
    
    // Query API for 16 days / daily forecast
    func forecast(_ location: String, temperature: TemperatureUnit = .celsius, count: Int = 5, completion: ((Response<Forecast, APIError>) -> Void)?) {
        
        // build url
        guard let apiKey = apiKey else {
            completion?(.failure(.missingAPIKey))
            return
        }
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.openweathermap.org"
        components.path = "/data/2.5/forecast/daily"
        components.queryItems = [
            URLQueryItem(name: "appid", value: apiKey),
            URLQueryItem(name: "q", value: location),
            URLQueryItem(name: "units", value: temperature.toAPIUnit()),
            URLQueryItem(name: "cnt", value: String(count + 1)),
            URLQueryItem(name: "mode", value: "json"),
        ]
        guard let url = components.url else {
            completion?(.failure(.buildingURL))
            return
        }
        
        query(url: url) { (result) in
            completion?(result)
        }
    }

    // Helper to query API for any URL
    private func query<T:Decodable>(url: URL, completion: ((Response<T, APIError>) -> Void)?) {

        URLSession.shared.dataTask(with: url) { (data, response, error) in

            if let error = error {
                completion?(.failure(.fetchingData(error: error)))
                return
            }
            
            guard let data = data else {
                completion?(.failure(.missingData))
                return
            }

            completion?(self.parse(data: data))
            
        }.resume()
    }

    // Helper to parse JSON returned by API
    private func parse<T:Decodable>(data: Data) -> (Response<T, APIError>) {
        
        let decoder = JSONDecoder()
        let result: T
        do {
            result = try decoder.decode(T.self, from: data)
        } catch {
            return .failure(.parsingJSON(error: error))
        }
        
        // if result has field "code" make sure the value is 200
        let mirror = Mirror(reflecting: result)
        if let i = mirror.children.index(where: { $0.label == "code" }) {
            if let code = mirror.children[i].value as? String {
                if code != "200" {
                    return .failure(.badCode(code: code))
                }
            }
        }
        
        return .success(result)
    }
    
}

