//
//  WeatherState.swift
//  weather
//
//  Created by Filipe Lemos on 12/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import Foundation

enum WeatherState {
    
    case sun
    case clouds
    case rain
    case storm
    case other
    
    static func from(code: Int) -> WeatherState {
        switch code {
        case 800:
            return .sun
        case 801...809:
            return .clouds
        case 500...599, 300...399:
            return .rain
        case 200...299:
            return .storm
        default:
            return .other
        }
    }
    
    func label() -> String {
        switch self {
        case .sun:
            return NSLocalizedString("Clear", comment: "")
        case .clouds:
            return NSLocalizedString("Clouds", comment: "")
        case .rain:
            return NSLocalizedString("Rain", comment: "")
        case .storm:
            return NSLocalizedString("Storm", comment: "")
        case .other:
            return ""
        }
    }

}


