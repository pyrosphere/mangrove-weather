//
//  Settings.swift
//  weather
//
//  Created by Filipe Lemos on 11/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import Foundation

class Settings {
    
    var location: String = "Rotterdam"
    var temperatureUnit: TemperatureUnit = .celsius
    var forecastDays: Int = 5
    
    static let shared: Settings = {
        let instance = Settings()
        return instance
    }()
    
    init() {}
    
    init(location: String, temperatureUnit: TemperatureUnit, forecastDays: Int) {
        self.location = location
        self.temperatureUnit = temperatureUnit
        self.forecastDays = forecastDays
    }
}

extension Settings: NSCopying {

    // MARK: NSCopying
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Settings(location: location, temperatureUnit: temperatureUnit, forecastDays: forecastDays)
        return copy
    }

}

extension Settings: Equatable {

    // MARK: Equatable

    static func ==(lhs: Settings, rhs: Settings) -> Bool {
        return lhs.location == rhs.location &&
                lhs.temperatureUnit == rhs.temperatureUnit &&
                lhs.forecastDays == rhs.forecastDays
    }

}
