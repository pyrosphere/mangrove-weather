//
//  TemperatureUnit.swift
//  weather
//
//  Created by Filipe Lemos on 12/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import Foundation

enum TemperatureUnit {
    
    case celsius
    case fahrenheit
    
    func toAPIUnit() -> String {
        switch self {
        case .celsius:
            return "metric"
        case .fahrenheit:
            return "imperial"
        }
    }
    
    func label() -> String {
        switch self {
        case .celsius:
            return NSLocalizedString("Celsius", comment: "")
        case .fahrenheit:
            return NSLocalizedString("Fahrenheit", comment: "")
        }
    }
    
}

