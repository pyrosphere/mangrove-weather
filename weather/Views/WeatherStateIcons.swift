//
//  WeatherStateIcons.swift
//  weather
//
//  Created by Filipe Lemos on 12/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import Foundation
import UIKit

extension WeatherState {
    
    func smallIcon() -> UIImage? {
        switch self {
        case .sun:
            return UIImage(named: "WeatherSmallSun")
        case .clouds:
            return UIImage(named: "WeatherSmallClouds")
        case .rain:
            return UIImage(named: "WeatherSmallRain")
        case .storm:
            return UIImage(named: "WeatherSmallStorm")
        default:
            return nil
        }
    }
    
    func largeIcon() -> UIImage? {
        switch self {
        case .sun:
            return UIImage(named: "WeatherLargeSun")
        case .clouds:
            return UIImage(named: "WeatherLargeClouds")
        case .rain:
            return UIImage(named: "WeatherLargeRain")
        case .storm:
            return UIImage(named: "WeatherLargeStorm")
        default:
            return nil
        }
    }
}
