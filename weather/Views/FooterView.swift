//
//  FooterView.swift
//  weather
//
//  Created by Filipe Lemos on 10/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class FooterView: NibView {
    
    @IBOutlet var label: UILabel!
    
    @IBInspectable var text: String? = nil{
        didSet{
            label.text = text
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        // show version in footer
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String {
            text = "v\(version)"
        }
    }

}

