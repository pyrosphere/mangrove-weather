//
//  CommonLabel.swift
//  weather
//
//  Created by Filipe Lemos on 12/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import UIKit

@IBDesignable
class CommonLabel: UILabel {
    var fontName: String? = {
        return nil
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        updateStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateStyle()
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        updateStyle()
    }
    
    func updateStyle() {}
}

class LabelRegular: CommonLabel {
    override func updateStyle() {
        font = UIFont(name: "Lato", size: font.pointSize)
    }
}

class LabelBold: CommonLabel {
    override func updateStyle() {
        font = UIFont(name: "Lato-Bold", size: font.pointSize)
    }
}

