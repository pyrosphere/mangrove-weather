//
//  DashboardViewCell.swift
//  weather
//
//  Created by Filipe Lemos on 10/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import UIKit

class DashboardViewCell: UITableViewCell {
    
    static let reuseIdentifier = "DashboardViewCell"
    
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var minTempLabel: UILabel!
    @IBOutlet var maxTempLabel: UILabel!
    @IBOutlet var weatherStateImageView: UIImageView!

}
