//
//  SettingsViewController.swift
//  weather
//
//  Created by Filipe Lemos on 10/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    @IBOutlet var footerView: FooterView!

    var tapDismissKeyboard: UITapGestureRecognizer!
    
    let reuseIdentifier = "SettingsCell"

    // settings in this view
    enum Setting {
        case location
        case temperatureUnits
        case forecastDays
        
        func label() -> String {
            switch self {
            case .location:
                return NSLocalizedString("Location", comment: "")
            case .temperatureUnits:
                return NSLocalizedString("Temperature units", comment: "")
            case .forecastDays:
                return NSLocalizedString("Forecast days", comment: "")
            }
        }
    }
    let settings:[Setting] = [.location, .temperatureUnits, .forecastDays]
    var editSetting:Setting? = nil
    let temperatureUnitsOptions:[TemperatureUnit] = [.fahrenheit, .celsius]
    let forecastDaysOptions:[Int] = Array(0...5)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // add gesture recognizer to dismiss keyboard on tap
        tapDismissKeyboard = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.dismissKeyboard(_:)))
        tapDismissKeyboard.numberOfTapsRequired = 1
        tapDismissKeyboard.numberOfTouchesRequired = 1
        tapDismissKeyboard.cancelsTouchesInView = false
        tapDismissKeyboard.isEnabled = false
        view.addGestureRecognizer(tapDismissKeyboard)
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        tapDismissKeyboard.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // sticky footer at the bottom
        let navHeight = (navigationController?.navigationBar.frame.height ?? 0) + (navigationController?.navigationBar.frame.origin.y ?? 0)
        let tableHeight = tableView.bounds.height - navHeight
        let footerHeight = footerView.frame.height
        let currentY = footerView.frame.origin.y
        let fixedY = tableHeight - footerHeight
        if currentY < fixedY {
            footerView.frame.origin.y = fixedY
        }
    }
    
    
    // - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    // - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        let setting = settings[indexPath.row]
        cell.textLabel?.text = setting.label()

        var detail:String
        switch setting {
        case .location:
            detail = Settings.shared.location
        case .temperatureUnits:
            detail = Settings.shared.temperatureUnit.label()
        case .forecastDays:
            detail = String(Settings.shared.forecastDays)
        }
        cell.detailTextLabel?.text = detail
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let setting = settings[indexPath.row]
        guard let cell = tableView.cellForRow(at: indexPath) else {
            return
        }
        editSetting = setting
        switch setting {
        case .location:
            inputLocation()
        case .temperatureUnits: fallthrough
        case .forecastDays:
            openPicker(cell: cell)
        }
    }
    
    func inputLocation() {
        
        let title = NSLocalizedString("Location", comment: "")
        let message = NSLocalizedString("Enter the city you want to receive the weather forecast for", comment: "")
        let placeholder = NSLocalizedString("City", comment: "")
        let confirmTitle = NSLocalizedString("OK", comment: "")
        let cancelTitle = NSLocalizedString("Cancel", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alertController.addTextField { (textField) in
            textField.placeholder = placeholder
        }

        let confirmAction = UIAlertAction(title: confirmTitle, style: .default) { (_) in
            let text = alertController.textFields?[0].text
            guard let city = text, city.count > 0 else {
                return
            }
            Settings.shared.location = city
            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
     
        self.present(alertController, animated: true, completion: nil)
    }
    
    func openPicker(cell: UITableViewCell) {

        guard let setting = editSetting else {
            return
        }
        var initialRow = 0
        switch setting {
        case .temperatureUnits:
            initialRow = temperatureUnitsOptions.index(of: Settings.shared.temperatureUnit) ?? 0
        case .forecastDays:
            initialRow = forecastDaysOptions.index(of: Settings.shared.forecastDays) ?? 0
        default:
            break
        }

        // get textfield for cell
        let view: UITextField? = cell.contentView.subviews.first(where: { $0 as? UITextField != nil }) as? UITextField
        guard let field = view else {
            return
        }

        // create picker as keyboard for textfield
        let pickerView = UIPickerView()
        pickerView.backgroundColor = UIColor.white
        pickerView.delegate = self
        pickerView.selectRow(initialRow, inComponent: 0, animated: false)
        field.inputView = pickerView
        field.becomeFirstResponder()
        tapDismissKeyboard.isEnabled = true
    }

}

extension SettingsViewController: UIPickerViewDataSource {

    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let setting = editSetting else {
            return 0
        }
        switch setting {
        case .temperatureUnits:
            return temperatureUnitsOptions.count
        case .forecastDays:
            return forecastDaysOptions.count
        default:
            return 0
        }
    }
}

extension SettingsViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let setting = editSetting else {
            return ""
        }
        switch setting {
        case .temperatureUnits:
            return temperatureUnitsOptions[row].label()
        case .forecastDays:
            return String(forecastDaysOptions[row])
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let setting = editSetting else {
            return
        }

        // update value in settings
        switch setting {
        case .temperatureUnits:
            Settings.shared.temperatureUnit = temperatureUnitsOptions[row]
        case .forecastDays:
            Settings.shared.forecastDays = forecastDaysOptions[row]
        default:
            break
        }

        // update value in cell
        guard let cellIndex = settings.index(of: setting),
            let cell = tableView.cellForRow(at: IndexPath(row: cellIndex, section: 0)) else {
                return
        }
        switch setting {
        case .temperatureUnits:
            cell.detailTextLabel?.text = temperatureUnitsOptions[row].label()
        case .forecastDays:
            cell.detailTextLabel?.text = String(forecastDaysOptions[row])
        default:
            break
        }

    }
}

