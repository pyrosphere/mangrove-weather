//
//  DashboardViewController.swift
//  weather
//
//  Created by Filipe Lemos on 09/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import UIKit

class DashboardViewController: UITableViewController {
    
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var weatherStateLabel: UILabel!
    @IBOutlet var weatherStateIcon: UIImageView!
    @IBOutlet var dayTempLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var maxTempLabel: UILabel!
    @IBOutlet var minTempLabel: UILabel!
    
    var forecast: WeatherAPIClient.Forecast? {
        didSet {
            self.updateLabels()
        }
    }

    var settings: Settings? // settings when the forecast was fetched
    
    var todayDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        //dateFormatter.setLocalizedDateFormatFromTemplate("EEE d MMM")
        dateFormatter.dateFormat = "EEE d MMM" // using fixed date format instead to follow sketch guidelines
        dateFormatter.shortWeekdaySymbols = dateFormatter.shortWeekdaySymbols.map { $0.localizedCapitalized }
        dateFormatter.shortMonthSymbols = dateFormatter.shortMonthSymbols.map { $0.localizedCapitalized }
        return dateFormatter
    }()
    
    var weekdayDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        dateFormatter.weekdaySymbols = dateFormatter.weekdaySymbols.map { $0.localizedCapitalized }
        return dateFormatter
    }()

    
    func updateLabels(error: WeatherAPIClient.APIError) {
        let errorText: String
        switch error {
        case .badCode(let code) where code == "404":
            errorText = NSLocalizedString("City not found", comment: "")
        default:
            errorText = NSLocalizedString("Error fetching data", comment: "")
        }
        weatherStateLabel.text = errorText
    }
    
    func updateLabels() {
        locationLabel.text = forecast?.city?.name ?? Settings.shared.location
        // update today
        if let today = forecast?.entries?[0] {
            weatherStateLabel.text = today.weather.label()
            weatherStateIcon.image = today.weather.largeIcon()
            dayTempLabel.text = "\(today.temperature.day.rounded())º"
            minTempLabel.text = "\(Int(today.temperature.min.rounded()))"
            maxTempLabel.text = "\(Int(today.temperature.max.rounded()))"
            dateLabel.text = todayDateFormatter.string(from: today.date)
        } else {
            weatherStateLabel.text = nil
            weatherStateIcon.image = nil
            dayTempLabel.text = nil
            minTempLabel.text = nil
            maxTempLabel.text = nil
            dateLabel.text = nil
        }
        // update following days
        tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero) // hide empty cells
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        // fetch new data if settings have changed
        if forecast == nil || settings != Settings.shared {
            settings = Settings.shared.copy() as? Settings
            forecast = nil
            WeatherAPIClient.shared.forecast(settings: Settings.shared) { (response) in
                DispatchQueue.main.async {
                    switch response {
                    case .failure(let error):
                        self.updateLabels(error: error)
                    case .success(let result):
                        self.forecast = result
                    }
                }
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max((forecast?.entries?.count ?? 0) - 1, 0)
    }
    
    // MARK: UITableViewDelegate

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DashboardViewCell.reuseIdentifier, for: indexPath)
        guard let dashboardCell = cell as? DashboardViewCell else {
            return cell
        }
        guard let entry = forecast?.entries?[indexPath.row + 1] else {
            return dashboardCell
        }
        if indexPath.row == 0 {
            dashboardCell.dayLabel.text = NSLocalizedString("Tomorrow", comment: "")
        } else {
            dashboardCell.dayLabel.text = weekdayDateFormatter.string(from: entry.date)
        }
        dashboardCell.minTempLabel.text = "\(Int(entry.temperature.min.rounded()))"
        dashboardCell.maxTempLabel.text = "\(Int(entry.temperature.max.rounded()))"
        dashboardCell.weatherStateImageView.image = entry.weather.smallIcon()
        return dashboardCell
    }
    
}


