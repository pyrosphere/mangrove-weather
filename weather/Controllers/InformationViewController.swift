//
//  InformationViewController.swift
//  weather
//
//  Created by Filipe Lemos on 10/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {
    
    @IBOutlet var footerView: FooterView!
    @IBOutlet var textLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // add line spacing
        if let text = textLabel.text {
            let attributedText = NSMutableAttributedString(string: text)
            let range = NSMakeRange(0, attributedText.length)
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 10
            style.paragraphSpacing = 5
            attributedText.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: range)
            textLabel.attributedText = attributedText
        }
        
    }

}
