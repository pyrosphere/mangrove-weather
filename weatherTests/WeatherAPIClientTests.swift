//
//  WeatherAPIClientTests.swift
//  weatherTests
//
//  Created by Filipe Lemos on 09/02/2018.
//  Copyright © 2018 Pyrosphere. All rights reserved.
//

import XCTest
@testable import weather

import Mockingjay

class WeatherAPIClientTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testForecastLondon() {
        
        // Tests forecast for "London" (local)
        
        let path = Bundle(for: type(of: self)).path(forResource: "ok_london", ofType: "json", inDirectory: "data")!
        let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
        XCTAssertNotNil(data, "Could not load local test data")
        stub(everything, jsonData(data!))
        
        let expectation = XCTestExpectation(description: "Forecast should return valid data")
        
        WeatherAPIClient.shared.forecast("London") { (response) in
            switch (response) {
            case .success(let result):
                
                XCTAssertEqual(result.code, "200")
                XCTAssertEqual(result.city?.name, "London")
                XCTAssertEqual(result.entries?[1].date, Date(timeIntervalSince1970: TimeInterval(1518523200)))
                XCTAssertEqual(result.entries?[0].weather, WeatherAPIClient.WeatherState.sun)
                XCTAssertEqual(result.entries?[1].weather, WeatherAPIClient.WeatherState.rain)

                expectation.fulfill()
                
            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
        }

        wait(for: [expectation], timeout: 2.0)
    }
    
    func testForecastNotFound() {
        
        // Tests forecast with empty data (local)

        let path = Bundle(for: type(of: self)).path(forResource: "error_not_found", ofType: "json", inDirectory: "data")!
        let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
        XCTAssertNotNil(data, "Could not load local test data")
        stub(everything, jsonData(data!))
        
        let expectation = XCTestExpectation(description: "Forecast should return 404")
        
        WeatherAPIClient.shared.forecast("London") { (response) in
            switch (response) {
            case .success:
                XCTFail("Should not be successful")
            case .failure(let error):
                switch error {
                case .badCode(let code):
                    XCTAssertEqual(code, "404")
                    expectation.fulfill()
                default:
                    XCTFail("Wrong error: \(error)")
                }
            }
        }
        
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testForecastBadJSON() {
        
        // Tests forecast with non JSON data (local)

        let data = "gibberish".data(using: .utf8)
        stub(everything, jsonData(data!))
        
        let expectation = XCTestExpectation(description: "Forecast should return JSON parse error")
        
        WeatherAPIClient.shared.forecast("London") { (response) in
            switch (response) {
            case .success:
                XCTFail("Should not be successful")
            case .failure(let error):
                switch error {
                case .parsingJSON:
                    expectation.fulfill()
                default:
                    XCTFail("Wrong error: \(error)")
                }
            }
        }
        
        wait(for: [expectation], timeout: 2.0)
    }

    func testForecastLondonLive() {
        
        // Tests forecast for "London" (live)
        
        let expectation = XCTestExpectation(description: "Forecast should return valid data")
        
        WeatherAPIClient.shared.forecast("London") { (response) in
            switch (response) {
            case .success(let result):
                
                XCTAssertEqual(result.code, "200")
                
                guard let city = result.city else {
                    XCTFail("Empty city")
                    return
                }
                
                guard let entries = result.entries, entries.count > 0 else {
                    XCTFail("Empty entries")
                    return
                }

                XCTAssertEqual(city.name, "London")

                expectation.fulfill()
                
            case .failure(let error):
                XCTFail("Unexpected error: \(error)")
            }
        }
        
        wait(for: [expectation], timeout: 10.0)

    }

}
